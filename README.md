# OChime
OChime is an Open Source Chime PCB replacement for those chimes from LCS. These
chimes where at one point available at [action](https://action.com) for €4,95.

In the end, this project is only reuse the housing, the speaker and buttons
of the original product. So then the original is basically an expensive
housing.


To view all project outputs of the
[master branch](https://olliver.gitlab.io/ochime/navigate/ochime-navigate.html)
or the
[interactive bill of materials](https://olliver.gitlab.io/ochime/ibom/ochime-ibom.html).

For release files, see the
[releases page](https://gitlab.com/olliver/ochime/-/releases) or the
[package repository](https://gitlab.com/olliver/ochime/-/packages).


## Hardware
The original chime lacks a few features, and is closed and proprietary, so
improving it is next to impossible. First of all, you can link the chime only
to a single doorbell. This is alright for the most common use-cases, but if
you want to use the chime for more purposes, e.g. 2 doorbells, one in the front
and one in the back. Or you want your smart home to also use the chimes, you
can't. What about having a chime on each floor? You can't.

To address this, the micro-controller would have to be reprogrammed, but it's
an unknown chip, maybe it cannot even be reprogrammed. The pinout suggests its
a PIC microcontroller, but who knows. So instead, lets just trans-plant the
entire PCB, and replace it with an ESP32-C2 super mini, external AMP and
optional external radio receiver. The ESP32-C2 mini was choosen as its one
of the smallest ESP's while still having enough pins.

Further more, the PCB was expanded to give a better fit to the second battery
connector, but while extending and offering more PCB space a screw-terminal
that can accept anything from 7 - 12 Volts DC, e.g. a standard doorbell
transformer.


## LCS
The brand on the chimes is LCS, but it is actually imported/distributed by
electrocirkel.com. These products are available under various brands such as
CALEX and Hema. They are also the smart-home products that can be found at
[action](https://action.com).
